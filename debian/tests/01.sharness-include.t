#!/bin/sh

set -e


test_description="verify if the sharness file can be sourced and its tests are usable"

. /usr/share/sharness/sharness.sh

test_expect_success "success" "
  true
"

test_expect_failure "failure" "
  false
"

test_done
